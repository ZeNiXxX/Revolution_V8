# AnyKernel3 Ramdisk Mod Script
# osm0sis @ xda-developers
# Used by Nulled_patronxx @ xda-developers

## AnyKernel setup
# begin properties
properties() { '
kernel.string=Revolution Kernel by Nulled_patronxx
do.devicecheck=1
do.modules=0
do.cleanup=1
do.cleanuponabort=1
device.name1=beryllium
device.name2=dipper
device.name3=polaris
device.name4=equuleus
device.name5=ursa
supported.versions=9,9.0,10,10.0
supported.patchlevels=
'; } # end properties

# shell variables
block=/dev/block/bootdevice/by-name/boot;
is_slot_device=0;
ramdisk_compression=auto;

## end install

## AnyKernel methods (DO NOT CHANGE)
# import patching functions/variables - see for reference
. tools/ak3-core.sh;

# Set Android version for kernel
ver="$(file_getprop /system/build.prop ro.build.version.release)"
if [ ! -z "$ver" ]; then
  patch_cmdline "androidboot.version" "androidboot.version=$ver"
else
  patch_cmdline "androidboot.version" ""
fi

## AnyKernel install
dump_boot;
if [ -d /data/adb/magisk ]; then
mkdir -p /data/adb/modules
else
ui_print "Magisk not installed."
fi
ui_print "Installing Revolution Kernel..."
write_boot;
